package com.diobulk.filmujo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.diobulk.filmujo.R.id.list_view;
/**
 * Created by E155282K on 29/03/17.
 */
public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
        public static final String URL = "https://api.themoviedb.org/3/search/movie?api_key=b2e256b3188558bc6988f6b0f5a59a18&include_adult=false&query=";

        private String request;
        private EditText mEditText;
        private Button button;
        private RequestQueue mRequestQueue;
        private String[] nbResultsStrings = {"All", "15 results", "10 results", "5 results", "Feeling lucky"};
        private Spinner spinner;
        private int nbResults = 20;
        private ProgressBar pb;
        private Intent i;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);

            // Calligraphy init
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );

            button = (Button) findViewById(R.id.button);
            mEditText = (EditText) findViewById(R.id.SearchEditText);
            spinner = (Spinner) findViewById(R.id.spinner);
            ArrayAdapter<String> adapter_nbResults = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, nbResultsStrings);
            spinner.setAdapter(adapter_nbResults);
            request = "";
            spinner.setOnItemSelectedListener(this);
            pb = (ProgressBar) findViewById(R.id.progressBar2);
            pb.setVisibility(View.GONE);

            // Request Queue
            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    sendRequest();
                }
            });

            mEditText.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                            case KeyEvent.KEYCODE_ENTER:
                                sendRequest();
                                return true;
                            default:
                                break;
                        }
                    }
                    return false;
                }
            });

        }

        // Required for Calligraphy
        @Override
        protected void attachBaseContext(Context newBase) {
            super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        }

        // If the user select nothing, 20 is the default value
        public void onNothingSelected(AdapterView<?> arg0) {
            nbResults = 20;
        }

        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            spinner.setSelection(position);
            String spinnerState = (String) spinner.getSelectedItem();
            switch (spinnerState) {
                case "All":
                    nbResults = 20;
                    break;
                case "15 results":
                    nbResults = 15;
                    break;
                case "10 results":
                    nbResults = 10;
                    break;
                case "5 results":
                    nbResults = 5;
                    break;
                case "Feeling lucky":
                    nbResults = 1;
                    break;
            }
        }

        private void sendRequest() {
            // Remove the keyboard when the button is clicked
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            pb.setVisibility(View.VISIBLE);
            request = mEditText.getText().toString();
            request = request.replaceAll("\\?", "");
            request = request.replaceAll("=", "");
            request = request.replaceAll("&", "and");
            request = request.replaceAll(" ", "+");

            if (!(request.isEmpty())) {
                // Send the request
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, URL + request,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if (nbResults == 1) {
                                        try {
                                            ParseJSON pj = new ParseJSON(response);
                                            if (pj.parseJSON(nbResults)) {
                                                JSONObject movie = ParseJSON.movies.getJSONObject(0);
                                                i = new Intent(HomeActivity.this, DetailsActivity.class);
                                                i.putExtra("movie", movie.toString());
                                                pb.setVisibility(View.GONE);
                                                startActivity(i);
                                            } else {
                                                Toast.makeText(getApplicationContext(), "No results", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {
                                            pb.setVisibility(View.GONE);
                                            Toast.makeText(getApplicationContext(), "Movie JSON error", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        try {
                                            Intent i = new Intent(HomeActivity.this, MainActivity.class);
                                            i.putExtra("response", response);
                                            pb.setVisibility(View.GONE);
                                            startActivity(i);
                                        } catch (Exception e) {
                                            pb.setVisibility(View.GONE);
                                            Toast.makeText(getApplicationContext(), "Intent error", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Request error, check connectivity", Toast.LENGTH_SHORT).show();
                            pb.setVisibility(View.GONE);
                        }
                    });
                    mRequestQueue.add(stringRequest);
                } else {
                    pb.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Enter something in the search field", Toast.LENGTH_SHORT).show();
                }
        }
    }

