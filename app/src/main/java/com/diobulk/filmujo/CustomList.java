package com.diobulk.filmujo;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by Mathis on 3/25/2017.
 * Permet d'adapter les données recus en String dans une ListView
 */
public class CustomList extends ArrayAdapter<String> {
    private String[] titles;
    private String[] dates;
    private Activity context;

    public CustomList(Activity context, String[] titles, String[] dates, int nbResults) {
        super(context, R.layout.list_view_layout, titles);
        this.context = context;
        this.titles = titles;
        this.dates = dates;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_view_layout, null, true);
        TextView textViewTitle = (TextView) listViewItem.findViewById(R.id.textViewTitle);
        TextView textViewDate = (TextView) listViewItem.findViewById(R.id.textViewDate);

        textViewTitle.setText(this.titles[position]);
        textViewDate.setText(this.dates[position]);

        return listViewItem;
    }
}