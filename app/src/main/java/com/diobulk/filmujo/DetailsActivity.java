package com.diobulk.filmujo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mathis on 3/26/2017.
 */

public class DetailsActivity extends AppCompatActivity {
    public static String URLImage = "https://image.tmdb.org/t/p/w500/";

    private ImageView imageView;
    private TextView textViewTitle;
    private TextView textViewDate;
    private TextView textViewDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewDesc = (TextView) findViewById(R.id.textViewDesc);

        // Restore the movie object from the last activity
        Intent intent = getIntent();
        String jsonString = intent.getStringExtra("movie");
        try {
            JSONObject movie = new JSONObject(jsonString);
            imageView = (ImageView) findViewById(R.id.imageView);
            Picasso.with(this)
                    .load(URLImage + movie.getString("poster_path"))
                    .into(imageView);
            textViewTitle.setText(movie.getString("title"));
            textViewDate.setText(movie.getString("release_date"));
            textViewDesc.setText(movie.getString("overview"));
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Movie JSON error Details", Toast.LENGTH_SHORT).show();
        }




    }
}
